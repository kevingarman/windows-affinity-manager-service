Imports System.Threading

Public Class MainService
    Inherits System.ServiceProcess.ServiceBase

    Private Shared pause As New ManualResetEvent(True)

    Private mainThread As Thread = Nothing

    Public Sub New()
        CanPauseAndContinue = True
        CanHandleSessionChangeEvent = False
        MyBase.ServiceName = My.Settings.serviceName
    End Sub

    Shared Sub Main()
        Try
            If System.Diagnostics.Debugger.IsAttached Then
                runningAsApplication = True
            End If

            Select Case LCase(Command())
                Case "/install"
                    ServiceInstall()
                    Exit Sub
                Case "/uninstall"
                    ServiceUninstall()
                    Exit Sub
                Case "/test"
                    runningAsApplication = True
                Case ""
                    'allow the service to run
                Case Else
                    MsgBox("Invalid command line argument")
                    Exit Sub
            End Select

            'load the service into memory.
            If runningAsApplication Then
                Dim args As String() = {""}
                Dim service As New MainService

                service.OnStart(args)

                Application.EnableVisualStyles()
                Application.Run(New frmMain)

                service.OnStop()
            Else
                'sleep for 10 seconds to wait for debugger to attach
                Thread.Sleep(10000)

                Dim servicesToRun() As System.ServiceProcess.ServiceBase
                servicesToRun = New System.ServiceProcess.ServiceBase() {New MainService}
                System.ServiceProcess.ServiceBase.Run(servicesToRun)
            End If

            appLog.Log("Service main method exiting...")

            ShutdownApp()

        Catch ex As Exception
            appLog.Log(ex.ToString)
            CrashDump(ex)
        End Try
    End Sub

    Private Shared Sub ShutdownApp()
        'last wishes
    End Sub

    Private Sub InitializeComponent()
        ' Initialize the operating properties for the service.
        Me.CanPauseAndContinue = True
        Me.CanShutdown = True
        Me.CanHandleSessionChangeEvent = False
        Me.ServiceName = "SimpleService"
    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            Dim handle As IntPtr = Me.ServiceHandle

            'start a separate thread that does the actual work.
            If mainThread Is Nothing OrElse (mainThread.ThreadState And System.Threading.ThreadState.Unstarted Or System.Threading.ThreadState.Stopped) <> 0 Then
                appLog.Log("Starting the service worker thread.")
            End If

            mainThread = New Thread(New ThreadStart(AddressOf ServiceWorkerMethod))
            mainThread.Name = My.Settings.serviceName & "MainThread"
            mainThread.Start()
            If Not (mainThread Is Nothing) Then
                appLog.Log("Worker thread state = " + mainThread.ThreadState.ToString())
            End If

        Catch ex As Exception
            appLog.Log(ex.ToString)
            CrashDump(ex)
        End Try
    End Sub

    Protected Overrides Sub OnStop()
        If runningAsApplication = False Then
            Me.RequestAdditionalTime(4000)
        End If

        'signal the worker thread to exit.
        If Not (mainThread Is Nothing) AndAlso mainThread.IsAlive Then
            appLog.Log("Stopping the service worker thread.")
            pause.Reset()
            'Thread.Sleep(2000)
            While mainThread.IsAlive
                Thread.Sleep(10)
            End While

            mainThread.Join()
        End If
        If Not (mainThread Is Nothing) Then
            appLog.Log("OnStop Worker thread state = " + mainThread.ThreadState.ToString())
        End If
        'indicate a successful exit.
        Me.ExitCode = 0

    End Sub 'OnStop

    'pause the service.
    Protected Overrides Sub OnPause()
        'pause the worker thread.
        If Not (mainThread Is Nothing) AndAlso mainThread.IsAlive AndAlso (mainThread.ThreadState And System.Threading.ThreadState.Suspended Or System.Threading.ThreadState.SuspendRequested) = 0 Then
            appLog.Log("Pausing the service worker thread.")
            pause.Reset()
        End If

        If Not (mainThread Is Nothing) Then
            appLog.Log("OnPause - Worker thread state = " + mainThread.ThreadState.ToString())
        End If

    End Sub

    'continue a paused service.
    Protected Overrides Sub OnContinue()

        'signal the worker thread to continue.
        If Not (mainThread Is Nothing) AndAlso (mainThread.ThreadState And System.Threading.ThreadState.Suspended Or System.Threading.ThreadState.SuspendRequested) <> 0 Then
            appLog.Log("Resuming the service worker thread.")
            pause.Set()
        End If
        If Not (mainThread Is Nothing) Then
            appLog.Log("OnContinue - Worker thread state = " + mainThread.ThreadState.ToString())
        End If

    End Sub

    'define a simple method that runs as the worker thread for the service.  
    Public Sub ServiceWorkerMethod()
        Dim sleepMillis As Integer = Math.Max(10000, My.Settings.sleepMillis)
        appLog.Log("Starting the service worker thread.")

        Dim am As New AffinityManager(My.Settings.processNames, My.Settings.affinityMask)
        Try
            Do
                am.AdjustAffinities()

                'block if the service is pausing or shutting down.
                If (pause.WaitOne(0, False) = False) Then
                    Debug.Print("Service.mainThread" & DateTime.Now.ToLongTimeString() + " - exiting.")
                    appLog.Log("Exiting the service worker thread.")
                    Exit Sub
                End If

                Thread.Sleep(sleepMillis)
            Loop While True
        Catch ex As Exception
            appLog.Log(ex.ToString)
        End Try
    End Sub

    Shared Sub CrashDump(ByVal ex As Exception)
        Dim strMessage As String = ex.ToString
        appLog.Log("Unexpected error, exiting... (" + strMessage + ")")

        System.Environment.Exit(1)
    End Sub
End Class
