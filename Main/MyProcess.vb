﻿Public Class MyProcess
    Implements IComparable

    Public mProcess As Process
    Public mCpuUsage As Integer

    Public Sub New(proc As Process, cpuUsage As Integer)
        mProcess = proc
        mCpuUsage = cpuUsage
    End Sub

    Public Function CompareTo(obj As Object) As Integer Implements IComparable.CompareTo
        Return mCpuUsage > CType(obj, MyProcess).mCpuUsage
    End Function
End Class
