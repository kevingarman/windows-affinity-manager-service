Module ServiceHelpers
    Private m_appPath As String
    Private m_appDir As String

    ''' <summary>
    ''' Returns the path, including filename, of the application, even when run as a service
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function AppPath() As String
        If m_appPath Is Nothing Then
            Dim tempPath As String
            tempPath = AppDomain.CurrentDomain.BaseDirectory & "\" & System.Reflection.Assembly.GetEntryAssembly.GetName.Name
            If tempPath.EndsWith(".exe") = False Then
                tempPath &= ".exe"
            End If
            m_appPath = tempPath
        End If
        Return m_appPath
    End Function

    ''' <summary>
    ''' Returns the path, excluding the filename, of the application, even when run as a service
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function AppDir() As String
        If m_appDir Is Nothing Then
            m_appDir = AppDomain.CurrentDomain.BaseDirectory
        End If
        Return m_appDir
    End Function

    Public Sub ServiceInstall()
        Dim path As String = AppPath()
        Try
            MsgBox("Installing: " & path)
            System.Configuration.Install.ManagedInstallerClass.InstallHelper(New String() {path})
        Catch ex As Exception
            appLog.Log(ex.ToString)
            MsgBox(ex.ToString)
        End Try
        Exit Sub
    End Sub

    Public Sub ServiceUninstall()
        Dim path As String = AppPath()
        Try
            System.Configuration.Install.ManagedInstallerClass.InstallHelper(New String() {"/u", path})
        Catch ex As Exception
            appLog.Log(ex.ToString)
            MsgBox(ex.ToString)
        End Try
        Exit Sub
    End Sub
End Module
