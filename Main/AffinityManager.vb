﻿Public Class AffinityManager
    Private mProcessNames() As String
    Private mMasks As New List(Of Integer)

    Public Sub New(processNames As String, affinityMask As Integer)
        mProcessNames = processNames.Split(",")

        Dim i As Integer = 1
        While i < 100
            If affinityMask And i Then
                mMasks.Add(i)
            End If
            i = i * 2
        End While
    End Sub

    Public Sub AdjustAffinities()
        Dim i As Integer
        Dim processes As New List(Of MyProcess)
        Dim perfCounter As New PerformanceCounter("Process", "% Processor Time")

        processes.Clear()
        For Each name As String In mProcessNames
            Dim tmp() As Process = Process.GetProcessesByName(name.Trim())
            For i = 0 To tmp.Length - 1
                perfCounter.InstanceName = GetProcessInstanceNameByPID(tmp(i).Id)
                Dim tmpP As New MyProcess(tmp(i), perfCounter.NextValue)
                processes.Add(tmpP)

                Debug.Print(tmpP.mProcess.ProcessName & " has affinity=" & tmpP.mProcess.ProcessorAffinity.ToString & " and cpu usage is " & tmpP.mCpuUsage & "%")
            Next
        Next

        processes.Sort()

        Debug.Print("Setting process affinites...")
        i = 0
        For Each p As MyProcess In processes
            Dim mask As Integer = 1
            If mMasks.Count > 0 Then
                mask = mMasks(i)
            End If
            p.mProcess.ProcessorAffinity = CType(mask, IntPtr)
            i += 1
            If i >= mMasks.Count Then
                i = 0
            End If
        Next
    End Sub

    Private Function GetProcessInstanceNameByPID(ByVal pid As Integer) As String
        Dim cat As New PerformanceCounterCategory("Process")
        Dim instances() As String = cat.GetInstanceNames()
        For Each instance As String In instances
            Using cnt As PerformanceCounter = New PerformanceCounter("Process", "ID Process", instance, True)
                Dim val As Integer = CType(cnt.RawValue, Int32)
                If val = pid Then
                    Return instance
                End If
            End Using
        Next
        Return Nothing
    End Function
End Class
