## Windows Affinity Manager Service

This is a simple service that periodically shuffles processes amongst CPUs to distribute CPU load.

# Usage

The service can be installed and uninstall by running as Administrator from the command line the following commands:

+ AffinityManagerService.exe /install
+ AffinityManagerService.exe /uninstall

# Configuration Settings

+ processNames - comma seperated list of processes that you wish to manage (ex. 'notepad' (without quotes) will manage the notepad.exe process)
+ affinityMask - integer that represents a bit mask indicating which CPUs you wish to use (ex. 1 will use CPU #0 and 4 will use CPUs #1 and #2)
+ sleepMillis - interval at which to set process affinities, minimum is 10000 (10sec)

# Precompiled Windows Binaries
Download [zipped binaries](https://bitbucket.org/kgmoney/windows-affinity-manager-service/downloads#download-182470).
