Imports System
Imports System.IO

''' <summary>Used to create and write to a data log stored as a text file</summary>
Public Class Logger
    Dim file As String
    Dim filesize As Integer
    Dim stream As FileStream
    Dim databuffer As New System.Collections.Generic.Queue(Of String)

    ''' <summary>Takes filename and creates filestream to load data to it. 
    ''' Creates a file if not located or appends to bottom of the found file.</summary>
    ''' <param name="filename">address of file</param>
    Public Sub New(ByVal filename As String)
        file = filename
        stream = New FileStream(file, FileMode.Append, FileAccess.Write, FileShare.Read)
        databuffer.Enqueue(vbCrLf & vbCrLf & Now.ToString("yyyy-MM-dd hh:mm:ss.ff") + "|Starting up..." + vbCrLf)
        WriteData()
    End Sub

    ''' <summary>Takes information and stores the data in collection and calls WriteData function.</summary>
    ''' <param name="message">Information to be Stored.</param>
    Public Sub Log(ByVal message As String)
        Console.WriteLine(message)
        databuffer.Enqueue(Now.ToString("yyyy-MM-dd hh:mm:ss.ff") + "|" + message.ToString + vbCrLf)
        WriteData()
    End Sub

    ''' <summary> Gets called by StoreData and writes the data in the collection to the predesignated file.
    ''' If file gets too big, calls on newlogfile to create new file.</summary>
    Private Sub WriteData()
        filesize = My.Computer.FileSystem.GetFileInfo(file).Length
        If databuffer.Count <> 0 Then
            Do
                Dim bytes As Byte() = New Text.UTF8Encoding().GetBytes(databuffer.Dequeue)
                filesize = filesize + bytes.Length
                stream.Write(bytes, 0, bytes.Length)
            Loop Until (databuffer.Count = 0)

            stream.Flush()
        End If

        If filesize > 5242880 Then
            newlogfile(file)
        End If
    End Sub

    ''' <summary> Gets called by WriteData when file gets too big. 
    ''' Moves old file out and changes its name based on when it is finished.
    ''' Creates new stream to new file.</summary>
    ''' <param name="file">Address of Text File.</param>
    Private Sub newlogfile(ByVal file As String)
        stream.Close()
        filesize = 0
        My.Computer.FileSystem.MoveFile(file, Now.ToString("yyyyMMddHHmmss") & "_" & file)
        stream = New FileStream(file, FileMode.Append, FileAccess.Write, FileShare.Read)
    End Sub
End Class

