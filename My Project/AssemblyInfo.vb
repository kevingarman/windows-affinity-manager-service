﻿Imports System.Resources

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("AffinityManagerService")> 
<Assembly: AssemblyDescription("Simple service to spread CPU load accross multiple CPUs")> 
<Assembly: AssemblyCompany("kgmoney.net")> 
<Assembly: AssemblyProduct("")> 
<Assembly: AssemblyCopyright("")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("17e493b1-8cbe-4007-875d-f896913df1e3")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.3.5")> 
<Assembly: AssemblyFileVersion("1.0.3.5")> 

<Assembly: NeutralResourcesLanguageAttribute("en")>
